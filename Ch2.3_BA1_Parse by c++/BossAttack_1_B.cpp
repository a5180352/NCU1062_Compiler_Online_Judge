#include <iostream>
#include <string>
#include <deque>
#include <utility>
using namespace std;

string input;
string err_msg;
deque<pair<string, string>> tokenStream;
int result;
bool eval_error = false;
bool num_caution = false;

bool ReadFormula();

int EvalFormula();
int Exp();

void PrintErrorMessage();

int main() {
    cout << "Welcome use our calculator!" << endl;
    cout << "> ";
    while(getline(cin, input)) {
        while (!input.empty() && input[0] == ' ') {
            input = input.substr(1, input.size()-1);
        }
        
        if (input.empty() && cin.eof()) {
            break;
        } else if (input.empty()) {
            cout << "> ";
            continue;
        }

        if (input[input.size()-1] == '\r') {
            input = input.substr(0, input.size()-1);
        }

        tokenStream.clear();
        eval_error = false;
        result = 0;
        err_msg = "";
        
        if (ReadFormula()) {
            result = EvalFormula();
            
            if (!eval_error) {
                if (tokenStream.empty()) {
                    cout << result << endl;
                } else {
                    cout << "Error: Illegal formula!" << endl;
                }
            } else {
                PrintErrorMessage();
            }
        } else {
            PrintErrorMessage();
        }

        if (cin.eof()) {
            break;
        }
        cout << "> ";
    }
    cout << "ByeBye~" << endl;
}

bool ReadFormula() {
    for (int i = 0; i < input.size();) {
        if (input[i] == '*' || input[i] == '/') {
            tokenStream.push_back(make_pair("OPERATOR", input.substr(i, 1)));
            i++;
            num_caution = false;
        } else if ((input[i] == '+' || input[i] == '-') && (input[i+1] >= '0' && input[i+1] <= '9')) {
            int len = 1;
            while(input[i+len] >= '0' && input[i+len] <= '9') {
                len++;
            }
            tokenStream.push_back(make_pair("INT", input.substr(i, len)));
            i += len;
            num_caution = true;
        } else if ((input[i] == '+' || input[i] == '-')) {
            tokenStream.push_back(make_pair("OPERATOR", input.substr(i, 1)));
            i++;
            num_caution = false;
        } else if (input[i] >= '0' && input[i] <= '9') {
            int len = 0;
            while(input[i+len] >= '0' && input[i+len] <= '9') {
                len++;
            }
            tokenStream.push_back(make_pair("INT", input.substr(i, len)));
            i += len;
            num_caution = true;
        } else if (input[i] == ' ') {
            i++;
            num_caution = false;
            continue;
        } else {
            int len = 0;
            while(i+len < input.size() && input[i+len] != ' ') {
                len++;
            }

            if (!tokenStream.empty() && num_caution) {
                err_msg = "Error: Unknown token " + tokenStream.back().second + input.substr(i, len);
            } else {
                err_msg = "Error: Unknown token " + input.substr(i, len);
            }
            return false;
        }
    }
    return true;
}

int EvalFormula() {
    return Exp();
}

int Exp() {
    if (!tokenStream.empty()){
        if (tokenStream.front().first == "OPERATOR") {
            char op = tokenStream.front().second[0];
            tokenStream.pop_front();
            int a = Exp();
            int b = Exp();

            switch (op) {
                case '+':
                    return a + b;
                case '-':
                    return a - b;
                case '*':
                    return a * b;
                case '/':
                    if (b == 0) {
                        eval_error = true;
                        err_msg = "Error: Divide by ZERO!";
                        return -1;
                    } 
                    return a / b;
            }
        } else if (tokenStream.front().first == "INT"){
            int tmp = atoi(tokenStream.front().second.c_str());
            tokenStream.pop_front();
            return tmp;
        }
    } else {
        err_msg = "Error: Illegal formula!";
        eval_error = true;
    }
}

void PrintErrorMessage() {
    cout << err_msg << endl;
}
