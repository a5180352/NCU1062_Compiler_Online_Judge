#include <iostream>
#include <string>
#include <vector>
#include <utility>
using namespace std;

string tmp;
string s = "";
vector<pair<string, string>> tokenStream;
bool error = false;

void removeFrontSpace();
bool peek(string t);
bool match(string t);

void program();
void stmts();
void stmt();
void exp();
void primary();
void primary_tail();

int main() {
    while(getline(cin , tmp)) {
        s = s + " " + tmp;
    }
    
    program();

    if (error) {
        cout << "invalid input" << endl;
    } else {
        for (unsigned int i = 0; i < tokenStream.size(); i++) {
            cout << tokenStream[i].first << " "
                 << tokenStream[i].second << endl;
        }
    }
}

void removeFrontSpace() {
    while (!s.empty() && s[0] == ' ') {
        s = s.substr(1, s.size()-1);
    }
}
bool peek(string t) {
    removeFrontSpace();
    if (t == "ID") {
        unsigned int id_length = 0;
        if (id_length < s.size() && ((s[id_length] >= 'A' && s[id_length] <= 'Z') ||
                                     (s[id_length] >= 'a' && s[id_length] <= 'z') ||
                                     s[id_length] == '_')) {
            id_length++;
            while (id_length < s.size() && ((s[id_length] >= 'A' && s[id_length] <= 'Z') ||
                                            (s[id_length] >= 'a' && s[id_length] <= 'z') ||
                                            (s[id_length] >= '0' && s[id_length] <= '9') ||
                                            s[id_length] == '_')) {
                id_length++;
            }
        }

        if (id_length != 0) {
            return true;
        }
    } else if (t == "STRLIT") {
        unsigned int str_length = 0;
        if (str_length+1 < s.size() && s[str_length] == '\"') {
            str_length++;
            while (str_length+1 < s.size() && s[str_length] != '\"') {
                str_length++;
            }
            str_length++;
        }

        if(str_length != 0) {
            return true;
        }
    } else if (t == "LBR") {
        if (!s.empty() && s[0] == '(') {
            return true;
        }
    } else if (t == "RBR") {
        if (!s.empty() && s[0] == ')') {
            return true;
        }
    } else if (t == "DOT") {
        if (!s.empty() && s[0] == '.') {
            return true;
        }
    } else if (t == "SEMICOLON") {
        if (!s.empty() && s[0] == ';') {
            return true;
        }
    }
    return false;
}
bool match(string t) {
    removeFrontSpace();
    if (t == "$") {
        if (s.empty()) {
            return true;
        }
    } else if (t == "ID") {
        unsigned int id_length = 0;
        if (id_length+1 < s.size() && ((s[id_length] >= 'A' && s[id_length] <= 'Z') ||
                                     (s[id_length] >= 'a' && s[id_length] <= 'z') ||
                                     s[id_length] == '_')) {
            id_length++;
            while (id_length+1 < s.size() && ((s[id_length] >= 'A' && s[id_length] <= 'Z') ||
                                            (s[id_length] >= 'a' && s[id_length] <= 'z') ||
                                            (s[id_length] >= '0' && s[id_length] <= '9') ||
                                            s[id_length] == '_')) {
                id_length++;
            }
        }

        if (id_length != 0) {
            tokenStream.push_back(make_pair(t, s.substr(0, id_length)));
            s = s.substr(id_length, s.size() - id_length);
            return true;
        }
    } else if (t == "STRLIT") {
        unsigned int str_length = 0;
        if (str_length+1 < s.size() && s[str_length] == '\"') {
            str_length++;
            while (str_length+1 < s.size() && s[str_length] != '\"') {
                str_length++;
            }
            str_length++;
        }

        if(str_length != 0) {
            tokenStream.push_back(make_pair(t, s.substr(0, str_length)));
            s = s.substr(str_length, s.size() - str_length);
            return true;
        }
    } else if (t == "LBR") {
        if (!s.empty() && s[0] == '(') {
            tokenStream.push_back(make_pair(t, s.substr(0, 1)));
            s = s.substr(1, s.size()-1);
            return true;
        }
    } else if (t == "RBR") {
        if (!s.empty() && s[0] == ')') {
            tokenStream.push_back(make_pair(t, s.substr(0, 1)));
            s = s.substr(1, s.size()-1);
            return true;
        }
    } else if (t == "DOT") {
        if (!s.empty() && s[0] == '.') {
            tokenStream.push_back(make_pair(t, s.substr(0, 1)));
            s = s.substr(1, s.size()-1);
            return true;
        }
    } else if (t == "SEMICOLON") {
        if (!s.empty() && s[0] == ';') {
            tokenStream.push_back(make_pair(t, s.substr(0, 1)));
            s = s.substr(1, s.size()-1);
            return true;
        }
    }
    error = true;
    return false;
}
void program() {
    stmts();
    match("$");
}
void stmts() {
    if (peek("ID") || peek("STRLIT")) {
        stmt();
        stmts();
    }
}
void stmt() {
    exp();
    match("SEMICOLON");
}
void exp() {
    if (peek("ID")) {
        primary();  
    } else if (peek("STRLIT")) {
        match("STRLIT");
    }
}
void primary() {
    match("ID");
    primary_tail();
}
void primary_tail() {
    if (peek("DOT")) {
        match("DOT");
        match("ID");
        primary_tail();
    } else if (peek("LBR")) {
        match("LBR");
        exp();
        match("RBR");
        primary_tail();
    }
}
