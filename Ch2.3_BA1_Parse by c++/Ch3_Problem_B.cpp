#include <iostream>
#include <string>
#include <sstream>
using namespace std;

int main() {
    string line_text;
    getline(cin, line_text);
    stringstream ss;
    ss << line_text;

    string column;
    int index = 0;
    while (ss >> column) {
        if (column == "Favorite") {
            break;
        }
        index++;
    }

    while (true) {
        getline(cin, line_text);
        if (cin.eof()) {
            break;
        }

        ss << line_text;
        for (int i = 0; i <= index; i++) {
            ss >> column;
        }
        if (column == "noodles") {
            cout << line_text << endl;
        }
    }
}