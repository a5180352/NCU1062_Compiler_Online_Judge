#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main() {
    string word;
    vector<string> texts;
    while (cin >> word) {
        if (cin.eof()) {
            break;
        } else {
            int start_index = 0;
            for (int i = 0; i < word.size(); i++) {
                if (word[i] == '(' || word[i] == ')' || word[i] == '{' || word[i] == '}' || word[i] == '/' || word[i] == ';') {
                    if (start_index != i) {
                        texts.push_back(word.substr(start_index, i - start_index));
                    }
                    start_index = i + 1;
                }
            }
            texts.push_back(word.substr(start_index, word.size() - start_index));
        }
    }

    for (int i = 0; i < texts.size(); i++) {
        string tmp = texts[i];
        unsigned found;
        unsigned keyword_pos = tmp.find("cpy");

        if (keyword_pos != -1) {
            if ((tmp[0] >= 'a' && tmp[0] <= 'z') ||
                (tmp[0] >= 'A' && tmp[0] <= 'Z') ||
                tmp[0] == '_' ||  tmp[0] == '$') {
                cout << tmp << endl;
            } else {
                continue;
            }
        } else {
            continue;
        }
    }
}