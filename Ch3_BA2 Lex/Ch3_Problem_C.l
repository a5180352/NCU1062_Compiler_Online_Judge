%option noyywrap
%{
    #include <iostream>
    using namespace std;
%}

digit   [0-9]
letter  [a-fA-F]

%%

^[ ]*0[xX]({letter}|{digit}){1,8}$ {
    while (*yytext == ' ') {
        yytext++;
    }
    cout << yytext << endl;
}
\n {
    // do nothing
}
. {
    // do nothing
}

%%

int main() {
    yylex();
}