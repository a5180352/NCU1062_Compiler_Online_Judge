%option noyywrap
%{
    #include <iostream>
    using namespace std;
%}

digit   [0-9]
SN      ^([\+-]?)([1-9]([\.]({digit})*)?)[E]([\+-]?)({digit})+$

%%

{SN} {
   cout << yytext << endl;
}
\n {
    // do nothing
}
. {
    // do nothing
}  

%%

int main() {
    yylex();
}