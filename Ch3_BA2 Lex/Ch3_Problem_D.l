%option noyywrap
%{
    #include <iostream>
    using namespace std;
%}

byte    [0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]
ip      {byte}\.{byte}\.{byte}\.{byte}

%%

{ip} {
   cout << yytext << endl;
}
\n {
    // do nothing
}
. {
    // do nothing
}  

%%

int main() {
    yylex();
}