%option noyywrap
%{
    #include <iostream>
    using namespace std;
%}

word    ^[A-Z][a-zA-Z]*$

byte    [0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]
ip      ^{byte}\.{byte}\.{byte}\.{byte}$

email   ^[a-zA-Z0-9]*@[a-zA-Z0-9]*(\.[a-zA-Z0-9]*)+$

%%

{word} {
    cout << yytext << " is a word" << endl;
}
{ip} {
   cout << yytext << " is an IP address" << endl;
}
{email} {
   cout << yytext << " is an email address" << endl;
}

\n {
    // do nothing
}
. {
    // do nothing
}

%%

int main() {
    yylex();
}