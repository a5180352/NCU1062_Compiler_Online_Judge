# NCU1062_Compiler_Online_Judge

* Lecture : NCU 1062 Compiler
* Language : `c++`
* Environment : Ubuntu 16.04 on Windows 10
* Tools : `g++`, `flex`, `bison`

---

1. **Parse by c++**
    * Ch2, Ch3, BossAttack_1
    * Use `c++` to solve some simple parse problem
2. **Lex**
    * Ch3, BossAttack_2
    * Use `flex` & `g++`
3. **Lex & Yacc**
    * Ch6, BossAttack_3
    * Use `bison` & `flex` & `g++`
    * Self practice : using [Lark](https://github.com/lark-parser/lark) parser in `python`