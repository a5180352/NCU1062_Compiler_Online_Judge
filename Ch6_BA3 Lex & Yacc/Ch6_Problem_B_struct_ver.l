%{
    #include "Ch6_Problem_B_struct_ver.tab.h"
    #include <string.h>
%}

%%

\n          { return 0;                                      }
[ \t]+      {                                                }

[0-9]+      { yylval.ival = atoi(yytext);    return INTEGER; }
[A-Z][a-z]? { yylval.cval = strdup(yytext);  return ELEMENT; }

"->"        { return ARROW;                                  }

"+"|"("|")" { return yytext[0];                              }

.           { return yytext[0];                              }

%%
