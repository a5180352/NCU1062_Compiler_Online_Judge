%{
    #include <iostream>
    #include <stack>
    using namespace std;

    void yyerror(const string error_msg);
    extern int yylex();

    stack<int> s;
    int op1, op2;

    void s_op2(string op);
    void s_op1(string op);
%}

%union {
    char* cval;
    int ival;
}

%token <ival> INTEGER
%token <cval> ADD
%token <cval> SUB
%token <cval> MUL
%token <cval> MOD
%token <cval> INC
%token <cval> DEC
%token <cval> LD

%type <cval> exprs
%type <cval> expr

%%

line    : exprs         { if (s.size() == 1) { cout << s.top();  exit(0); } }
        ;   
exprs   : expr exprs
        | expr
        ;
expr    : ADD           { s_op2("add"); }
        | SUB           { s_op2("sub"); }
        | MUL           { s_op2("mul"); }
        | MOD           { s_op2("mod"); }
        | INC           { s_op1("inc"); }
        | DEC           { s_op1("dec"); }
        | LD INTEGER    { s.push($2);   }
        ;

%%

int main(int argc, char *argv[]) {
    yyparse();
    yyerror("");
}

void yyerror(const string error_msg) {
    cout << "Invalid format" << endl;
    exit(0);
}

void s_op2(string op) {
    if (s.size() >= 2) {
        op1 = s.top(); s.pop();
        op2 = s.top(); s.pop();
        if      (op == "add") op1 = op1 + op2;
        else if (op == "sub") op1 = op1 - op2;
        else if (op == "mul") op1 = op1 * op2;
        else if (op == "mod") op1 = op1 % op2;
        s.push(op1);
    } else {
        yyerror("");
    }
}

void s_op1(string op) {
    if (s.size() >= 1) {
        if (op == "inc") s.top() += 1;
        else             s.top() -= 1;
    } else {
        yyerror("");
    }
}