%{
    #include "Ch6_Problem_A.tab.h"
    #include <string.h>
    int pos = 0;
%}

%%

\n                  { return(0); /* EOF */                                          }
[ \t]+              { pos += yyleng;                                                }

[0-9]+              { pos += yyleng;  yylval.ival = atoi(yytext);  return(INTEGER); }
"["|"]"|","         { pos += yyleng;  return(yytext[0]);                            }

"^T"                { pos += yyleng;  return(TRANSPOSE);                            }

"+"|"-"|"*"|"("|")" { yylval.ival = pos;  pos += yyleng;  return(yytext[0]);        }

.                   { pos += yyleng;  return(yytext[0]);                            }

%%
