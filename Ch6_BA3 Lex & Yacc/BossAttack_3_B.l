%{
    #include "BossAttack_3_B.tab.h"
    #include <string.h>
%}

%%

\n      {                                              }
[ \t]+  {                                              }

[0-9]+  { yylval.ival = atoi(yytext);  return INTEGER; }

"add"   { return ADD;                                  }
"sub"   { return SUB;                                  }
"mul"   { return MUL;                                  }
"mod"   { return MOD;                                  }

"inc"   { return INC;                                  }
"dec"   { return DEC;                                  }

"load"  { return LD;                                   }

.       { return yytext[0];                            }

%%
