%code requires {
    #include <iostream>
    #include <map>
    using namespace std;

    void yyerror(const string error_msg);
    extern int yylex();

    void print_map(map<string, int> &target);
    void map_multi(map<string, int> &target, int coef);
    void add_to(map<string, int> &src, map<string, int> &dst, int option);

    typedef struct {
        char* cval;
        int ival;
        map<string, int> m;
    } node;
    #define YYSTYPE node
}

%token <ival> INTEGER
%token <cval> ELEMENT
%token <cval> ARROW

%type <m> expr
%type <m> chems
%type <m> chem
%type <m> eles
%type <m> ele

%%

line    : expr                  { print_map($1);                         }
        ;
expr    : chems ARROW chems     { $$ = $1;  add_to($3, $$, -1);          }
        ;
chems   : chem '+' chems        { $$ = $3;  add_to($1, $$, 1);           }
        | chem                  { $$ = $1;                               }
        ;
chem    : INTEGER eles          { map_multi($2, $1);  $$ = $2;           }
        | eles                  {                     $$ = $1;           }
        ;
eles    : ele eles              { $$ = $2;  add_to($1, $$, 1);           }
        | ele                   { $$ = $1;                               }
        ;
ele     : ELEMENT INTEGER       { $$[$1] += $2;                          }
        | ELEMENT               { $$[$1] += 1;                           }
        | '(' eles ')' INTEGER  { map_multi($2, $4);  add_to($2, $$, 1); }
        | '(' eles ')'          {                     add_to($2, $$, 1); }
        ;

%%

int main(int argc, char *argv[]) {
    yyparse();
}

void yyerror(const string error_msg) {
    cout << "Invalid format" << endl;
    exit(0);
}

void print_map(map<string, int> &target) {
    for (map<string, int>::iterator it = target.begin(); it != target.end(); it++)
        if (it->second != 0)
            cout << it->first << " " << it->second << endl;
}

void map_multi(map<string, int> &target, int coef) {
    for(map<string, int>::iterator it = target.begin(); it != target.end(); it++)
        it->second *= coef;
}

void add_to(map<string, int> &src, map<string, int> &dst, int option = 1) {
    for(map<string, int>::iterator it = src.begin(); it != src.end(); it++)
        dst[it->first] += it->second * option;
}