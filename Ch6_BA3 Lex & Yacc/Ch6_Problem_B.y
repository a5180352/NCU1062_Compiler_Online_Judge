%{
    #include <iostream>
    #include <map>
    using namespace std;

    void yyerror(const string error_msg);
    extern int yylex();

    void chem_0_to_handside(bool lhs);
    void chem_multi(int level, int coef);
    void chem_to_chem(int src_lvl, int dst_lvl);

    map<string, int> map_lhs, map_rhs, map_chem[10]; // brackets' deepth
    unsigned int current_level = 0;
    bool is_left = true;
%}

%union {
    char* cval;
    int ival;
}

%token <ival> INTEGER
%token <cval> ELEMENT
%token <cval> ARROW

%%

line    : expr
        ;
expr    : chems arrow chems
        ;
chems   : chem '+' chems
        | chem
        ;
chem    : INTEGER eles  { chem_multi(0, $1);  chem_0_to_handside(is_left); }
        | eles          {                     chem_0_to_handside(is_left); }
        ;
eles    : ele eles
        | ele
        ;
ele     : ELEMENT INTEGER        { map_chem[current_level][$1] += $2; }
        | ELEMENT                { map_chem[current_level][$1] += 1;  }
        | left eles ')' INTEGER  { chem_multi(current_level, $4);  chem_to_chem(current_level, current_level-1);  current_level--; }
        | left eles ')'          {                                 chem_to_chem(current_level, current_level-1);  current_level--; }
        ;
arrow   : ARROW  { is_left = false; }
        ;
left    : '('    { current_level++; }
        ;

%%

int main(int argc, char *argv[]) {
    yyparse();
    // left_hand_side = left_hand_side - right_hand_side 
    for (map<string, int>::iterator it = map_rhs.begin(); it != map_rhs.end(); it++)
        map_lhs[it->first] -= it->second;
    // print left_hand_side
    for (map<string, int>::iterator it = map_lhs.begin(); it != map_lhs.end(); it++)
        if (it->second != 0)
            cout << it->first << " " << it->second << endl;
}

void yyerror(const string error_msg) {
    cout << "Invalid format" << endl;
    exit(0);
}

void chem_0_to_handside(bool lhs) {
    if (lhs)
        for(map<string, int>::iterator it = map_chem[0].begin(); it != map_chem[0].end(); it++)
            map_lhs[it->first] += it->second;
    else
        for(map<string, int>::iterator it = map_chem[0].begin(); it != map_chem[0].end(); it++)
            map_rhs[it->first] += it->second;
    map_chem[0].clear();
}

void chem_multi(int level, int coef) {
    for(map<string, int>::iterator it = map_chem[level].begin(); it != map_chem[level].end(); it++)
        it->second *= coef;
}

void chem_to_chem(int src_lvl, int dst_lvl) {
    for(map<string, int>::iterator it = map_chem[src_lvl].begin(); it != map_chem[src_lvl].end(); it++)
        map_chem[dst_lvl][it->first] += it->second;
    map_chem[src_lvl].clear();
}