%{
    #include <stdio.h>
    #include <math.h>
    #include <iostream>
    using namespace std;

    void yyerror(const string error_msg);
    extern int yylex();
%}

%union {
    char* cval;
    int ival;
    double dval;
}

%token <ival> INTEGER
%token <cval> FRAC

%type <dval> expr
%type <dval> term

%%

line    : expr              { printf("%.3f\n", $1); }
        ;
expr    : expr '+' term     { $$ = $1 + $3; }
        | expr '-' term     { $$ = $1 - $3; }
        | term              { $$ = $1;      }
        ;
term    : '{' expr '}'                      { $$ = $2;          }
        | FRAC '{' expr '}' '{' expr '}'    { $$ = $3 / $6;     }
        | term '^' term                     { $$ = pow($1, $3); }
        | INTEGER                           { $$ = (double)$1;  }
        ;

%%

int main(int argc, char *argv[]) {
    yyparse();
}

void yyerror(const string error_msg) {
    cout << "Invalid format" << endl;
    exit(0);
}
