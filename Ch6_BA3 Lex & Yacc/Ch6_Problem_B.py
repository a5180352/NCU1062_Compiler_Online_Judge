from lark import Lark, Transformer

f = open('Ch6_Problem_B.lark', 'r')
grammer = f.read()
parser = Lark(grammer)

class Chemicals(Transformer):
    def ele(self, items):
        length = len(items)
        if length == 1:
            return {str(items[0]): 1}
        else:
            return {str(items[0]): int(items[1])}

    def bracket(self, items):
        for k in items[0]:
            items[0][k] *= int(items[1])
        return items[0]

    def eles(self, items):
        for key in items[0]:
            if items[1].get(key) == None:
                items[1][key] = items[0][key]
            else:
                items[1][key] += items[0][key]
        return items[1]

    def chem(self, items):
        for key in items[1]:
            items[1][key] *= int(items[0])
        return items[1]

    def chems(self, items):
        for key in items[0]:
            if items[1].get(key) == None:
                items[1][key] = items[0][key]
            else:
                items[1][key] += items[0][key]
        return items[1]
    
    def expr(self, items):
        # print('LHS : ', items[0])
        # print('RHS : ', items[1])
        for key in items[1]:
            if items[0].get(key) == None:
                items[0][key] = -items[1][key]
            else:
                items[0][key] -= items[1][key]
        for key in sorted(items[0]):
            if items[0][key] != 0:
                print(key, items[0][key])

if __name__ == '__main__':
    s = input()
    try:
        tree = parser.parse(s)
        # print(tree.pretty())
        try:
            Chemicals().transform(tree)
        except:
            print('Transformer error')
    except:
        print('Invalid format')