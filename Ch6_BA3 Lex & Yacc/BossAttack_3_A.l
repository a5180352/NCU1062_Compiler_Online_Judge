%{
    #include "BossAttack_3_A.tab.h"
    #include <string.h>
%}

%%

\n                  { return 0;                                    }
[ \t]+              {                                              }

[0-9]+              { yylval.ival = atoi(yytext);  return INTEGER; }
[\\]frac            { return FRAC;                                 }

"+"|"-"|"^"|"{"|"}" { return yytext[0];                            }

.                   { return yytext[0];                            }

%%
