%code requires {
    #include <stdlib.h>
    #include <iostream>
    using namespace std;

    typedef struct {
        int rows;
        int cols;
    } mat;

    void yyerror(const string error_msg);
    extern int yylex();

    void mat_trans(mat &src, mat &dst);
    void mat_multi(mat &src1, mat &src2, mat &dst, int pos);
    void mat_plus(mat &src1, mat &src2, mat &dst, int pos);
}

%union {
    char* cval;
    int ival;
    mat m;
}

%token <ival> INTEGER
%token <cval> TRANSPOSE

%type <m> expr
%type <m> term
%type <ival> '+' '-' '*'

%left '*'

%%

line    : expr                          { cout << "Accepted" << endl;  }
        ;
expr    : expr '+' term                 { mat_plus($1, $3, $$, $2);    }
        | expr '-' term                 { mat_plus($1, $3, $$, $2);    }
        | term
        ;
term    : '(' expr ')'                  { $$ = $2;                     }
        | term TRANSPOSE                { mat_trans($1, $$);           }
        | term '*' term                 { mat_multi($1, $3, $$, $2);   }
        | '[' INTEGER ',' INTEGER ']'   { $$.rows = $2;  $$.cols = $4; }
        ;

%%

int main(int argc, char *argv[]) {
    yyparse();
}

void yyerror(const string error_msg) {
    cout << "Syntax Error" << endl;
}

void mat_trans(mat &src, mat &dst) {
    dst.rows = src.cols;
    dst.cols = src.rows;
}

void mat_multi(mat &src1, mat &src2, mat &dst, int pos) {
    if (src1.cols == src2.rows) {
        dst.rows = src1.rows;
        dst.cols = src2.cols;
    } else {
        cout << "Semantic error on col " << pos + 1 << endl;
        exit(0);
    }
}

void mat_plus(mat &src1, mat &src2, mat &dst, int pos) {
    if (src1.rows == src2.rows && src1.cols == src2.cols) {
        dst.rows = src1.rows;
        dst.cols = src1.cols;
    } else {
        cout << "Semantic error on col " << pos + 1 << endl;
        exit(0);
    }
}